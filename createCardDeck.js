/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const suits = ["hearts", "spades", "clubs", "diamonds"];
const values = [2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11];
const displayVals = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"];

const getDeck = () => {
  const deck = new Array();
  
  for(let i = 0; i < suits.length; i++)
  {
    for(let j = 0; j < values.length; j++)
    {
      const cards = {
        val: values[j],
        displayVal: displayVals[j],
        suit: suits[i]
      }
      deck.push(cards.val);
    }
  }
  return deck;
};


// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
  randomCard.displayVal &&
  typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);